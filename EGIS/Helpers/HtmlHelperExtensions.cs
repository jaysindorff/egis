﻿using System.DirectoryServices.AccountManagement;
using System.Web.Mvc;

namespace Egis.Helpers
{
    public static class HtmlHelperExtensions
    {
        public static string GetUserSID(this HtmlHelper helper, string userName)
        {
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain);
            return UserPrincipal.FindByIdentity(ctx, userName).Sid.ToString();
        }

        public static string GetUserFullName(this HtmlHelper helper, string userName)
        {
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain);
            return UserPrincipal.FindByIdentity(ctx, userName).GivenName.ToString() + " " + UserPrincipal.FindByIdentity(ctx, userName).Surname.ToString();
        }
    }
}