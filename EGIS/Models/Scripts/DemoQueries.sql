SET QUOTED_IDENTIFIER OFF;
GO
USE [EGIS-Demo];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

/* NOTE: The user queries below are based on the SID of the account that created the 
         record in the database, and not the 'owner' of the issue/case/action/item.
		 These scripts are intended to be used to demonstrate the UI functionality
		 of EGIS, and the output from these scripts will need to be modified once 
		 real logic starts to be developed.*/

-- Information Elements for Review
-- User No 1
SELECT [Title], [DateDocumentReceived]
FROM [InformationElements] ie
LEFT JOIN [InformationElementReviews] ier
  ON ie.InformationElementId = ier.InformationElementId
LEFT JOIN [Users] u
  ON ie.Tracking_SID = u.[SID]
WHERE ier.InformationElementId IS NULL
  AND u.SamAccountName = "DolsN" 
ORDER BY ie.[DateDocumentReceived] ASC
-- User No 2
SELECT [Title], [DateDocumentReceived]
FROM [InformationElements] ie
LEFT JOIN [InformationElementReviews] ier
  ON ie.InformationElementId = ier.InformationElementId
LEFT JOIN [Users] u
  ON ie.Tracking_SID = u.[SID]
WHERE ier.InformationElementId IS NULL
  AND u.SamAccountName = "RuttanA" 
ORDER BY ie.[DateDocumentReceived] ASC

-- Case Summary Screen 
-- User 1
SELECT [CaseCategory], COUNT([CaseCategory]) AS TotalCases
FROM [CDRCases] c
LEFT JOIN [Users] u
  ON c.Tracking_SID = u.[SID]
WHERE u.SamAccountName = "DolsN" 
GROUP BY [CaseCategory]
ORDER BY [CaseCategory]
-- User 2
SELECT [CaseCategory], COUNT([CaseCategory]) AS TotalCases
FROM [CDRCases] c
LEFT JOIN [Users] u
  ON c.Tracking_SID = u.[SID]
WHERE u.SamAccountName = "RuttanA" 
GROUP BY [CaseCategory]
ORDER BY [CaseCategory]

-- Outstanding Actions
-- User 1
SELECT [IssueType], [FollowUpDate]
FROM [CDRIssues]
