SET QUOTED_IDENTIFIER OFF;
GO
USE [EGIS-Demo];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO


-- RoleTypes
DELETE FROM [RoleTypes]
SET IDENTITY_INSERT [RoleTypes] ON

INSERT INTO [RoleTypes] ([RoleTypeId], [Role]) VALUES(0, "Unknown");
INSERT INTO [RoleTypes] ([RoleTypeId], [Role]) VALUES(1, "Witness to incident");
INSERT INTO [RoleTypes] ([RoleTypeId], [Role]) VALUES(2, "Person Deceased in the same incident");
INSERT INTO [RoleTypes] ([RoleTypeId], [Role]) VALUES(3, "Person responsible for supervision of the child");
INSERT INTO [RoleTypes] ([RoleTypeId], [Role]) VALUES(4, "Driver of the vehicle in which deceased was travelling");
INSERT INTO [RoleTypes] ([RoleTypeId], [Role]) VALUES(5, "Driver of other vehicle involved in death incident");
INSERT INTO [RoleTypes] ([RoleTypeId], [Role]) VALUES(6, "Alleged perpetrator of harm or maltreatment");
INSERT INTO [RoleTypes] ([RoleTypeId], [Role]) VALUES(7, "Deceased child linked to contagion or cluster suicide");
INSERT INTO [RoleTypes] ([RoleTypeId], [Role]) VALUES(8, "Child co-bathing with deceased child");
INSERT INTO [RoleTypes] ([RoleTypeId], [Role]) VALUES(9, "Child identified as requiring postvention support");
INSERT INTO [RoleTypes] ([RoleTypeId], [Role]) VALUES(10, "No role in death incident");

SET IDENTITY_INSERT [RoleTypes] OFF

-- RelationshipTypes
DELETE FROM [RelationshipTypes] 
SET IDENTITY_INSERT [RelationshipTypes] ON

INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(0, "Familial", "Parent", "Mother", "Child")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(1, "Familial", "Parent", "Father", "Child")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(2, "Familial", "Parent", "Step Mother", "Step Child")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(3, "Familial", "Parent", "Step Father", "Step Child")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(4, "Familial", "Parent", "Mothers non-cohabitating Partner", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(5, "Familial", "Parent", "Fathers non-cohabitating Partner", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(6, "Familial", "Parent", "Parents previous Spouse/Partner", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(7, "Familial", "Parent", "Other", "Other")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(8, "Familial", "Sibling", "Sibling", "Sibling")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(9, "Familial", "Sibling", "Step Sibling", "Step Sibling")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(10, "Familial", "Sibling", "Half Sibling", "Half Sibling")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(11, "Familial", "Sibling", "Other", "Other")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(12, "Familial", "Grandparent", "Maternal Grandmother", "Maternal Grandchild")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(13, "Familial", "Grandparent", "Maternal Grandfather", "Maternal Grandchild")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(14, "Familial", "Grandparent", "Paternal Grandmother", "Paternal Grandchild")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(15, "Familial", "Grandparent", "Paternal Grandfather", "Paternal Grandchild")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(16, "Familial", "Grandparent", "Step Grandmother", "Step Grandchild")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(17, "Familial", "Grandparent", "Step Grandfather", "Step Grandchild")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(18, "Familial", "Grandparent", "Other", "Other")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(19, "Familial", "Other Relative", "Cousin", "Cousin")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(20, "Familial", "Other Relative", "Paternal Aunt", "Paternal Neice/Nephew")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(21, "Familial", "Other Relative", "Paternal Uncle", "Paternal Neice/Nephew")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(22, "Familial", "Other Relative", "Maternal Aunt", "Maternal Neice/Nephew")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(23, "Familial", "Other Relative", "Maternal Uncle", "Maternal Neice/Nephew")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(24, "Familial", "Other Relative", "Neice", "Uncle/Aunt")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(25, "Familial", "Other Relative", "Nephew", "Uncle/Aunt")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(26, "Familial", "Other Relative", "Child", "Parent")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(27, "Familial", "Other Relative", "Other", "Other")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(28, "Non-Familial", "Carer", "Residential Carer", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(29, "Non-Familial", "Carer", "Foster Carer", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(30, "Non-Familial", "Carer", "Other", "Other")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(31, "Non-Familial", "Other Child in Household", "", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(32, "Non-Familial", "Peer", "Friend", "Friend")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(33, "Non-Familial", "Peer", "Acquaintance", "Acquaintance")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(34, "Non-Familial", "Peer", "Work Colleague", "Work Colleague")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(35, "Non-Familial", "Peer", "Other", "Other")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(36, "Non-Familial", "Intimate Partner", "Boyfriend", "Boyfriend/Girlfriend")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(37, "Non-Familial", "Intimate Partner", "Girlfriend", "Boyfriend/Girlfriend")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(38, "Non-Familial", "Intimate Partner", "Defacto/Spouse", "Defacto/Spouse")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(39, "Non-Familial", "Intimate Partner", "Other", "Other")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(40, "Non-Familial", "Other", "Healthcare Provider", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(41, "Non-Familial", "Other", "Mental Healthcare Provider", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(42, "Non-Familial", "Other", "Police Officer", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(43, "Non-Familial", "Other", "Ambulance Officer", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(44, "Non-Familial", "Other", "Child Safety Officer", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(45, "Non-Familial", "Other", "Community Officer", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(46, "Non-Familial", "Other", "Community Elder", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(47, "Non-Familial", "Other", "Teacher", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(48, "Non-Familial", "Other", "Guidance Counsellor", "")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(49, "Non-Familial", "Other", "Employer", "Employee")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(50, "Non-Familial", "Other", "Neighbour", "Neighbour")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(51, "Non-Familial", "Other", "Unknown Relationship", "Unknown Relationship")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(52, "Non-Familial", "Other", "Unknown to Child", "Unknown to Child")
INSERT INTO [RelationshipTypes] ([RelationshipTypeId], [Type], [SubType], [SpecificType], [SpecificTypeReverse]) VALUES(53, "Non-Familial", "Other", "", "")

SET IDENTITY_INSERT [RelationshipTypes] OFF

-- People
DECLARE @ID NUMERIC; SET @ID = 0
DELETE FROM [People]

INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Myrtice", "Curimao", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Bertram", "Emshwiller", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Harralson", "Merksamer", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Leesa", "Wassermann", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Valentine", "Hoerger", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Fermin", "Melchert", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Tamar", "Kahao", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Donte", "Hilda", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Mckinley", "Rodamis", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Huey", "Bursik", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Markus", "Biederwolf", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Kenyatta", "Kearsey", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Alena", "Patanella", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Whitney", "Vaughner", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Liane", "Kuzemchak", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Huey", "Caire", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Renea", "Pleskac", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Theo", "Kotte", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Huey", "Skyes", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Palma", "Tinin", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Rosaria", "Guiel", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Corrina", "Vainio", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Carri", "Machle", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Corrina", "Yaklich", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Jamey", "Ettel", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Sunshine", "Ruckel", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Adalberto", "Warran", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Chet", "Sarzynski", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Judie", "Simplot", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Karlene", "Subich", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Carolann", "Lasaint", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Julissa", "Kleifgen", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Karlene", "Placino", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Ouida", "Biard", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Kristyn", "Fratta", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Christiane", "Kominek", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Huey", "Goepel", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Lou", "Rastegar", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Omer", "Ledebuhr", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Renea", "Kietzer", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Fiona", "Lidge", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Huey", "Esmon", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Kecia", "Heidi", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Maximo", "Matusz", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Christiane", "Jakiela", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Shantell", "Yonek", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Barrett", "Kluber", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Palma", "Fischhaber", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Hipolito", "Splonskowski", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Isaias", "Harfert", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Mckinley", "Gellinger", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Lorelei", "Lenberg", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Kathryne", "Rebik", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Kathryne", "Stengle", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Rashida", "Nistendirk", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Theo", "Osowicz", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Lucienne", "Israels", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Hipolito", "Macclellan", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Sherron", "Hartory", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Isaias", "Putcha", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Geraldo", "Makhija", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Jamey", "Hanoa", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Juliann", "Slayman", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Bertram", "Stoett", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Martine", "Cendana", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Juliann", "Olecki", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Huey", "Umnus", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Marylee", "Falencki", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Donte", "Slingland", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Chung", "Riller", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Sherron", "Racanello", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Tyron", "Manby", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Markus", "Maixner", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Bo", "Menger", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Kristyn", "Hedeiros", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Barrett", "Burfeind", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Danica", "Montaivo", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Adalberto", "Willington", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Siobhan", "Lipstone", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Anglea", "Remaly", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Paola", "Larko", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Bo", "Circelli", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Jenniffer", "Gruba", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Tyron", "Romanoff", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Teodoro", "Jostes", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Theo", "Soorus", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Hilton", "Hape", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Charlette", "Fechtig", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Maximo", "Neundorfer", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Valentine", "Parsens", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "In", "Boughn", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Liane", "Kylish", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Sherron", "Pecht", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Lou", "Wittry", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Mckinley", "Shilkuski", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Bertram", "Popec", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Columbus", "Stathes", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Rosaria", "Spragley", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Fermin", "Nedina", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Siobhan", "Rieske", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Barrett", "Kveton", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Julieta", "Parlet", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Stefani", "Kanda", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Adelia", "Knopinski", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Meryl", "Hatzenbihler", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Angella", "Jecmenek", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Martine", "Semenick", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Kenna", "Steindorf", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Lou", "Sikander", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Donte", "Konetchy", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Charlette", "Moehle", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Barrett", "Leonberger", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Kristyn", "Hendeson", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Isaias", "Boldery", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;
INSERT INTO [People] ([PersonId], [FamilyName], [GivenNames], [DateOfBirth], [TimeStamp]) VALUES (@ID, "Dwain", "Naifeh", GETDATE() - CAST(RAND() * RAND() / RAND() * 155 * 18 AS NUMERIC) , GETDATE()); SET @ID = @ID+1;



