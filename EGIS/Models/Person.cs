//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace EGIS.Models
{
    public partial class Person
    {
        public int PersonId { get; set; }
        public string FamilyName { get; set; }
        public string GivenNames { get; set; }
        public System.DateTime DateOfBirth { get; set; }
        public Nullable<System.DateTime> DateOfDeathFrom { get; set; }
        public Nullable<System.DateTime> DateOfDeathTo { get; set; }
        public Nullable<int> IndigenousCommunityId { get; set; }
        public Nullable<int> OtherIndigenousCommunityId { get; set; }
        public Nullable<int> PrimaryLanguageId { get; set; }
        public Nullable<int> SecondaryLanguageId { get; set; }
        public Nullable<int> CountryOfBirthId { get; set; }
        public string PersonData { get; set; }
        public string TimeStamp { get; set; }
    }
    
}
