//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace EGIS.Models
{
    public partial class Tracking
    {
        public System.Guid SID { get; set; }
        public System.TimeSpan TimeStamp { get; set; }
    }
    
}
