﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EGIS.Models
{
    public class Analytics
    {
        public IEnumerable<EGIS.Models.vInformationElementsForReview> InformationElementsForReview { get; set; }
        public IEnumerable<EGIS.Models.vCaseSummary> OpenCases { get; set; }
        public IEnumerable<EGIS.Models.vIssuesActions> IssuesActions { get; set; }
    }
}