﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EGIS.Models
{
    public class Location
    {
        public int Id { get; set; }
        public string BuildingName { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string Suburb { get; set; }
        public string Postcode { get; set; }
        public List<LocationCategory> LocationCategories { get; set; }

    }
}