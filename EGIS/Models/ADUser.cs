﻿using System.Collections.Generic;
namespace EGIS.Models
{
    public class ADUser
    {
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string VoiceTelephoneNumber { get; set; }
        public string UserId { get; set; }
        public string SID { get; set; }
        public bool IsUser { get; set; }
        public List<UserRole> Roles { get; set; }
    }
}