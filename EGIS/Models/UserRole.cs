﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EGIS.Models
{
    public class UserRole
    {
        [UIHint("UserRole")]
        public string RoleName { get; set; }
    }
}