
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 05/09/2012 14:10:34
-- Generated from EDMX file: C:\Users\BurgessA\Documents\Visual Studio 2010\Projects\EGIS\EGIS\Models\DataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [EGIS-Demo];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_DocumentTypeInformationElement]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InformationElements] DROP CONSTRAINT [FK_DocumentTypeInformationElement];
GO
IF OBJECT_ID(N'[dbo].[FK_InformationElementInformationElementReview]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InformationElementReviews] DROP CONSTRAINT [FK_InformationElementInformationElementReview];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Aliases]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Aliases];
GO
IF OBJECT_ID(N'[dbo].[CDRActions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CDRActions];
GO
IF OBJECT_ID(N'[dbo].[CDRCases]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CDRCases];
GO
IF OBJECT_ID(N'[dbo].[CDRIssues]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CDRIssues];
GO
IF OBJECT_ID(N'[dbo].[DocumentTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DocumentTypes];
GO
IF OBJECT_ID(N'[dbo].[InformationElementReviews]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InformationElementReviews];
GO
IF OBJECT_ID(N'[dbo].[InformationElements]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InformationElements];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[DataModelStoreContainer].[vCaseSummaries]', 'U') IS NOT NULL
    DROP TABLE [DataModelStoreContainer].[vCaseSummaries];
GO
IF OBJECT_ID(N'[DataModelStoreContainer].[vCaseSummaries1]', 'U') IS NOT NULL
    DROP TABLE [DataModelStoreContainer].[vCaseSummaries1];
GO
IF OBJECT_ID(N'[DataModelStoreContainer].[vInformationElementsForReviews]', 'U') IS NOT NULL
    DROP TABLE [DataModelStoreContainer].[vInformationElementsForReviews];
GO
IF OBJECT_ID(N'[DataModelStoreContainer].[vInformationElementsForReviews1]', 'U') IS NOT NULL
    DROP TABLE [DataModelStoreContainer].[vInformationElementsForReviews1];
GO
IF OBJECT_ID(N'[DataModelStoreContainer].[vCaseSummary]', 'U') IS NOT NULL
    DROP TABLE [DataModelStoreContainer].[vCaseSummary];
GO
IF OBJECT_ID(N'[DataModelStoreContainer].[vInformationElementsForReview]', 'U') IS NOT NULL
    DROP TABLE [DataModelStoreContainer].[vInformationElementsForReview];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Aliases'
CREATE TABLE [dbo].[Aliases] (
    [AliasPersonId] int IDENTITY(1,1) NOT NULL,
    [PersonId] int  NOT NULL,
    [FamilyName] nvarchar(max)  NOT NULL,
    [GivenNames] nvarchar(max)  NOT NULL,
    [Tracking_SID] uniqueidentifier  NOT NULL,
    [Tracking_TimeStamp] time  NOT NULL
);
GO

-- Creating table 'CDRIssues'
CREATE TABLE [dbo].[CDRIssues] (
    [CDRIssueId] int IDENTITY(1,1) NOT NULL,
    [Status] int  NOT NULL,
    [CreationDate] datetime  NOT NULL,
    [IssueType] nvarchar(max)  NOT NULL,
    [Tracking_SID] uniqueidentifier  NOT NULL,
    [Tracking_TimeStamp] time  NOT NULL,
    [FollowUpRequired] bit  NOT NULL,
    [FollowUpDate] datetime  NULL
);
GO

-- Creating table 'CDRActions'
CREATE TABLE [dbo].[CDRActions] (
    [CDRActionId] int IDENTITY(1,1) NOT NULL,
    [ActionType] nvarchar(max)  NOT NULL,
    [DateActionCompleted] datetime  NULL,
    [Tracking_SID] uniqueidentifier  NOT NULL,
    [Tracking_TimeStamp] time  NOT NULL
);
GO

-- Creating table 'InformationElements'
CREATE TABLE [dbo].[InformationElements] (
    [InformationElementId] int IDENTITY(1,1) NOT NULL,
    [ReferenceNo] nvarchar(max)  NULL,
    [ReferenceSystem] nvarchar(max)  NULL,
    [DocumentTypeId] int  NOT NULL,
    [Title] nvarchar(max)  NOT NULL,
    [DateCreated] datetime  NOT NULL,
    [DateOfDocument] datetime  NULL,
    [DateDocumentReceived] datetime  NOT NULL,
    [DocumentLink] nvarchar(max)  NOT NULL,
    [DataFormat] nvarchar(max)  NOT NULL,
    [Medium] nvarchar(max)  NOT NULL,
    [Tracking_SID] uniqueidentifier  NOT NULL,
    [Tracking_TimeStamp] time  NOT NULL,
    [ActionTitle] nvarchar(max)  NULL,
    [ActionDate] datetime  NULL,
    [IssueTitle] nvarchar(max)  NULL,
    [IssueDate] datetime  NULL,
    [Category] nvarchar(max)  NULL,
    [Topic] nvarchar(max)  NULL
);
GO

-- Creating table 'InformationElementReviews'
CREATE TABLE [dbo].[InformationElementReviews] (
    [InformationElementId] int IDENTITY(1,1) NOT NULL,
    [Tracking_SID] uniqueidentifier  NOT NULL,
    [Tracking_TimeStamp] time  NOT NULL,
    [InformationElement_InformationElementId] int  NOT NULL
);
GO

-- Creating table 'DocumentTypes'
CREATE TABLE [dbo].[DocumentTypes] (
    [DocumentTypeId] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(max)  NOT NULL,
    [Class] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [UserEntityId] int IDENTITY(1,1) NOT NULL,
    [SamAccountName] nvarchar(max)  NOT NULL,
    [FullName] nvarchar(max)  NOT NULL,
    [DisplayName] nvarchar(max)  NOT NULL,
    [SID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'CDRCases'
CREATE TABLE [dbo].[CDRCases] (
    [CDRCaseId] int IDENTITY(1,1) NOT NULL,
    [Tracking_SID] uniqueidentifier  NOT NULL,
    [Tracking_TimeStamp] time  NOT NULL,
    [CaseCategory] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'vInformationElementsForReviews'
CREATE TABLE [dbo].[vInformationElementsForReviews] (
    [Title] nvarchar(max)  NOT NULL,
    [DateDocumentReceived] datetime  NOT NULL,
    [Topic] nvarchar(max)  NULL,
    [Action] nvarchar(max)  NULL,
    [DateCreated] datetime  NULL,
    [Requesting_Organisation] nvarchar(max)  NULL,
    [AreedDeliveryDate] datetime  NULL,
    [ReferralRecipient] nvarchar(max)  NULL
);
GO

-- Creating table 'vCaseSummary1'
CREATE TABLE [dbo].[vCaseSummary1] (
    [CaseCategory] nvarchar(max)  NOT NULL,
    [TotalCases] int  NULL
);
GO

-- Creating table 'vCaseSummaries1'
CREATE TABLE [dbo].[vCaseSummaries1] (
    [CaseCategory] nvarchar(max)  NOT NULL,
    [TotalCases] int  NULL
);
GO

-- Creating table 'vInformationElementsForReview1'
CREATE TABLE [dbo].[vInformationElementsForReview1] (
    [Title] nvarchar(max)  NOT NULL,
    [DateDocumentReceived] datetime  NOT NULL
);
GO

-- Creating table 'vInformationElementsForReviews1'
CREATE TABLE [dbo].[vInformationElementsForReviews1] (
    [Title] nvarchar(max)  NOT NULL,
    [DateDocumentReceived] datetime  NOT NULL
);
GO

-- Creating table 'People'
CREATE TABLE [dbo].[People] (
    [PersonId] int  NOT NULL,
    [FamilyName] nvarchar(max)  NOT NULL,
    [GivenNames] nvarchar(max)  NOT NULL,
    [DateOfBirth] datetime  NOT NULL,
    [DateOfDeathFrom] datetime  NULL,
    [DateOfDeathTo] datetime  NULL,
    [IndigenousCommunityId] int  NULL,
    [OtherIndigenousCommunityId] int  NULL,
    [PrimaryLanguageId] int  NULL,
    [SecondaryLanguageId] int  NULL,
    [CountryOfBirthId] int  NULL,
    [PersonData] nvarchar(max)  NULL,
    [TimeStamp] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'DeceasedChildren'
CREATE TABLE [dbo].[DeceasedChildren] (
    [DeceasedChildId] int  NOT NULL,
    [RegistrationDate] datetime  NOT NULL,
    [AgeAtTimeOfDeath] smallint  NOT NULL,
    [AgeCategory] nvarchar(max)  NULL,
    [Sex] nvarchar(max)  NOT NULL,
    [Neonatal] bit  NOT NULL,
    [PostNeonatal] bit  NOT NULL,
    [ReligionId] smallint  NULL,
    [DeceasedChildData] nvarchar(max)  NULL,
    [TimeStamp] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Relationships'
CREATE TABLE [dbo].[Relationships] (
    [DeceasedChildId] int  NOT NULL,
    [PersonId] int  NOT NULL,
    [RelationshipTypeId] int  NULL,
    [RelationshipInfoId] int  NOT NULL,
    [RelationshipData_RelationshipInfoId] int  NOT NULL,
    [RelationshipType_RelationshipTypeId] int  NOT NULL
);
GO

-- Creating table 'RelationshipRoles'
CREATE TABLE [dbo].[RelationshipRoles] (
    [DeceasedChildId] int  NOT NULL,
    [PersonId] int  NOT NULL,
    [RoleTypeId] int  NOT NULL,
    [RoleData] nvarchar(max)  NULL,
    [Relationship_DeceasedChildId] int  NOT NULL,
    [Relationship_PersonId] int  NOT NULL,
    [RoleType_RoleTypeId] int  NOT NULL
);
GO

-- Creating table 'RelationshipInformation'
CREATE TABLE [dbo].[RelationshipInformation] (
    [RelationshipInfoId] int IDENTITY(1,1) NOT NULL,
    [RelationshipData] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'RelationshipTypes'
CREATE TABLE [dbo].[RelationshipTypes] (
    [RelationshipTypeId] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(max)  NOT NULL,
    [SubType] nvarchar(max)  NOT NULL,
    [SpecificType] nvarchar(max)  NOT NULL,
    [SpecificTypeReverse] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'RoleTypes'
CREATE TABLE [dbo].[RoleTypes] (
    [RoleTypeId] int IDENTITY(1,1) NOT NULL,
    [Role] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [AliasPersonId] in table 'Aliases'
ALTER TABLE [dbo].[Aliases]
ADD CONSTRAINT [PK_Aliases]
    PRIMARY KEY CLUSTERED ([AliasPersonId] ASC);
GO

-- Creating primary key on [CDRIssueId] in table 'CDRIssues'
ALTER TABLE [dbo].[CDRIssues]
ADD CONSTRAINT [PK_CDRIssues]
    PRIMARY KEY CLUSTERED ([CDRIssueId] ASC);
GO

-- Creating primary key on [CDRActionId] in table 'CDRActions'
ALTER TABLE [dbo].[CDRActions]
ADD CONSTRAINT [PK_CDRActions]
    PRIMARY KEY CLUSTERED ([CDRActionId] ASC);
GO

-- Creating primary key on [InformationElementId] in table 'InformationElements'
ALTER TABLE [dbo].[InformationElements]
ADD CONSTRAINT [PK_InformationElements]
    PRIMARY KEY CLUSTERED ([InformationElementId] ASC);
GO

-- Creating primary key on [InformationElementId] in table 'InformationElementReviews'
ALTER TABLE [dbo].[InformationElementReviews]
ADD CONSTRAINT [PK_InformationElementReviews]
    PRIMARY KEY CLUSTERED ([InformationElementId] ASC);
GO

-- Creating primary key on [DocumentTypeId] in table 'DocumentTypes'
ALTER TABLE [dbo].[DocumentTypes]
ADD CONSTRAINT [PK_DocumentTypes]
    PRIMARY KEY CLUSTERED ([DocumentTypeId] ASC);
GO

-- Creating primary key on [UserEntityId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([UserEntityId] ASC);
GO

-- Creating primary key on [CDRCaseId] in table 'CDRCases'
ALTER TABLE [dbo].[CDRCases]
ADD CONSTRAINT [PK_CDRCases]
    PRIMARY KEY CLUSTERED ([CDRCaseId] ASC);
GO

-- Creating primary key on [Title], [DateDocumentReceived] in table 'vInformationElementsForReviews'
ALTER TABLE [dbo].[vInformationElementsForReviews]
ADD CONSTRAINT [PK_vInformationElementsForReviews]
    PRIMARY KEY CLUSTERED ([Title], [DateDocumentReceived] ASC);
GO

-- Creating primary key on [CaseCategory] in table 'vCaseSummary1'
ALTER TABLE [dbo].[vCaseSummary1]
ADD CONSTRAINT [PK_vCaseSummary1]
    PRIMARY KEY CLUSTERED ([CaseCategory] ASC);
GO

-- Creating primary key on [CaseCategory] in table 'vCaseSummaries1'
ALTER TABLE [dbo].[vCaseSummaries1]
ADD CONSTRAINT [PK_vCaseSummaries1]
    PRIMARY KEY CLUSTERED ([CaseCategory] ASC);
GO

-- Creating primary key on [Title], [DateDocumentReceived] in table 'vInformationElementsForReview1'
ALTER TABLE [dbo].[vInformationElementsForReview1]
ADD CONSTRAINT [PK_vInformationElementsForReview1]
    PRIMARY KEY CLUSTERED ([Title], [DateDocumentReceived] ASC);
GO

-- Creating primary key on [Title], [DateDocumentReceived] in table 'vInformationElementsForReviews1'
ALTER TABLE [dbo].[vInformationElementsForReviews1]
ADD CONSTRAINT [PK_vInformationElementsForReviews1]
    PRIMARY KEY CLUSTERED ([Title], [DateDocumentReceived] ASC);
GO

-- Creating primary key on [PersonId] in table 'People'
ALTER TABLE [dbo].[People]
ADD CONSTRAINT [PK_People]
    PRIMARY KEY CLUSTERED ([PersonId] ASC);
GO

-- Creating primary key on [DeceasedChildId] in table 'DeceasedChildren'
ALTER TABLE [dbo].[DeceasedChildren]
ADD CONSTRAINT [PK_DeceasedChildren]
    PRIMARY KEY CLUSTERED ([DeceasedChildId] ASC);
GO

-- Creating primary key on [DeceasedChildId], [PersonId] in table 'Relationships'
ALTER TABLE [dbo].[Relationships]
ADD CONSTRAINT [PK_Relationships]
    PRIMARY KEY CLUSTERED ([DeceasedChildId], [PersonId] ASC);
GO

-- Creating primary key on [DeceasedChildId], [PersonId] in table 'RelationshipRoles'
ALTER TABLE [dbo].[RelationshipRoles]
ADD CONSTRAINT [PK_RelationshipRoles]
    PRIMARY KEY CLUSTERED ([DeceasedChildId], [PersonId] ASC);
GO

-- Creating primary key on [RelationshipInfoId] in table 'RelationshipInformation'
ALTER TABLE [dbo].[RelationshipInformation]
ADD CONSTRAINT [PK_RelationshipInformation]
    PRIMARY KEY CLUSTERED ([RelationshipInfoId] ASC);
GO

-- Creating primary key on [RelationshipTypeId] in table 'RelationshipTypes'
ALTER TABLE [dbo].[RelationshipTypes]
ADD CONSTRAINT [PK_RelationshipTypes]
    PRIMARY KEY CLUSTERED ([RelationshipTypeId] ASC);
GO

-- Creating primary key on [RoleTypeId] in table 'RoleTypes'
ALTER TABLE [dbo].[RoleTypes]
ADD CONSTRAINT [PK_RoleTypes]
    PRIMARY KEY CLUSTERED ([RoleTypeId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [DocumentTypeId] in table 'InformationElements'
ALTER TABLE [dbo].[InformationElements]
ADD CONSTRAINT [FK_DocumentTypeInformationElement]
    FOREIGN KEY ([DocumentTypeId])
    REFERENCES [dbo].[DocumentTypes]
        ([DocumentTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DocumentTypeInformationElement'
CREATE INDEX [IX_FK_DocumentTypeInformationElement]
ON [dbo].[InformationElements]
    ([DocumentTypeId]);
GO

-- Creating foreign key on [InformationElement_InformationElementId] in table 'InformationElementReviews'
ALTER TABLE [dbo].[InformationElementReviews]
ADD CONSTRAINT [FK_InformationElementInformationElementReview]
    FOREIGN KEY ([InformationElement_InformationElementId])
    REFERENCES [dbo].[InformationElements]
        ([InformationElementId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InformationElementInformationElementReview'
CREATE INDEX [IX_FK_InformationElementInformationElementReview]
ON [dbo].[InformationElementReviews]
    ([InformationElement_InformationElementId]);
GO

-- Creating foreign key on [DeceasedChildId] in table 'DeceasedChildren'
ALTER TABLE [dbo].[DeceasedChildren]
ADD CONSTRAINT [FK_PersonDeceasedChild]
    FOREIGN KEY ([DeceasedChildId])
    REFERENCES [dbo].[People]
        ([PersonId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [DeceasedChildId] in table 'Relationships'
ALTER TABLE [dbo].[Relationships]
ADD CONSTRAINT [FK_DeceasedChildRelationship]
    FOREIGN KEY ([DeceasedChildId])
    REFERENCES [dbo].[DeceasedChildren]
        ([DeceasedChildId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [PersonId] in table 'Relationships'
ALTER TABLE [dbo].[Relationships]
ADD CONSTRAINT [FK_PersonRelationship]
    FOREIGN KEY ([PersonId])
    REFERENCES [dbo].[People]
        ([PersonId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonRelationship'
CREATE INDEX [IX_FK_PersonRelationship]
ON [dbo].[Relationships]
    ([PersonId]);
GO

-- Creating foreign key on [Relationship_DeceasedChildId], [Relationship_PersonId] in table 'RelationshipRoles'
ALTER TABLE [dbo].[RelationshipRoles]
ADD CONSTRAINT [FK_RelationshipRelationshipRole]
    FOREIGN KEY ([Relationship_DeceasedChildId], [Relationship_PersonId])
    REFERENCES [dbo].[Relationships]
        ([DeceasedChildId], [PersonId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RelationshipRelationshipRole'
CREATE INDEX [IX_FK_RelationshipRelationshipRole]
ON [dbo].[RelationshipRoles]
    ([Relationship_DeceasedChildId], [Relationship_PersonId]);
GO

-- Creating foreign key on [RelationshipData_RelationshipInfoId] in table 'Relationships'
ALTER TABLE [dbo].[Relationships]
ADD CONSTRAINT [FK_RelationshipDataRelationship]
    FOREIGN KEY ([RelationshipData_RelationshipInfoId])
    REFERENCES [dbo].[RelationshipInformation]
        ([RelationshipInfoId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RelationshipDataRelationship'
CREATE INDEX [IX_FK_RelationshipDataRelationship]
ON [dbo].[Relationships]
    ([RelationshipData_RelationshipInfoId]);
GO

-- Creating foreign key on [RelationshipType_RelationshipTypeId] in table 'Relationships'
ALTER TABLE [dbo].[Relationships]
ADD CONSTRAINT [FK_RelationshipTypeRelationship]
    FOREIGN KEY ([RelationshipType_RelationshipTypeId])
    REFERENCES [dbo].[RelationshipTypes]
        ([RelationshipTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RelationshipTypeRelationship'
CREATE INDEX [IX_FK_RelationshipTypeRelationship]
ON [dbo].[Relationships]
    ([RelationshipType_RelationshipTypeId]);
GO

-- Creating foreign key on [RoleType_RoleTypeId] in table 'RelationshipRoles'
ALTER TABLE [dbo].[RelationshipRoles]
ADD CONSTRAINT [FK_RoleTypeRelationshipRole]
    FOREIGN KEY ([RoleType_RoleTypeId])
    REFERENCES [dbo].[RoleTypes]
        ([RoleTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RoleTypeRelationshipRole'
CREATE INDEX [IX_FK_RoleTypeRelationshipRole]
ON [dbo].[RelationshipRoles]
    ([RoleType_RoleTypeId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------