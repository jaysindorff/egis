﻿using System.Web.Mvc;
using System.Web.Security;
using EGIS.Models;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using Telerik.Web.Mvc;

namespace EGIS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to EGIS!";

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult BusinessViewSettings()
        {
            return View();
        }

        [HttpPost]
        public ActionResult BusinessViewSettings(string theme)
        {
            return View();
        }

        public ActionResult SystemAdministration()
        {
            return View();
        }

        public ActionResult LocationManagement()
        {
            LocationManagement lm = new LocationManagement();
            lm.Locations = new List<Location>();
            Location location = new Location() { Id = 1, BuildingName = "The Mansion", Postcode = "4012", Street1 = "Street st", Street2 = "", Suburb = "Coorparoo" };
            location.LocationCategories = new List<LocationCategory>();
            LocationCategory lc = new LocationCategory() { Id = 1, CategoryName = "Place of usual Residence" };
            lc.RelatedData = new List<RelatedDataItem>();
            lc.RelatedData.Add(new RelatedDataItem() { Key = "Housing Services Property", Value = "Yes" });
            lc.RelatedData.Add(new RelatedDataItem() { Key = "Evidence being rented", Value = "Yes" });
            lc.RelatedData.Add(new RelatedDataItem() { Key = "Length of time rented", Value = "2 years" });
            location.LocationCategories.Add(lc);
            lm.Locations.Add(location);
            location = new Location() { Id = 2, BuildingName = "", Postcode = "4014", Street1 = "Another Street st", Street2 = "", Suburb = "Manly" };
            location.LocationCategories = new List<LocationCategory>();
            lc = new LocationCategory() { Id = 2, CategoryName = "Other Known residential location" };
            lc.RelatedData = new List<RelatedDataItem>();
            lc.RelatedData.Add(new RelatedDataItem() { Key = "Details of other known residential location", Value = "Only on the weekends" });
            location.LocationCategories.Add(lc);
            lm.Locations.Add(location);
            location = new Location() { Id = 2, BuildingName = "", Postcode = "4123", Street1 = "Bruce Highway", Street2 = "", Suburb = "Nambour" };
            location.LocationCategories = new List<LocationCategory>();
            lc = new LocationCategory() { Id = 5, CategoryName = "Death Incident location" };
            lc.RelatedData = new List<RelatedDataItem>();
            lc.RelatedData.Add(new RelatedDataItem() { Key = "Road Condition", Value = "Dry, Good visibility" });
            lc.RelatedData.Add(new RelatedDataItem() { Key = "Carriageway", Value = "Yes" });
            lc.RelatedData.Add(new RelatedDataItem() { Key = "Area Speed limit", Value = "100 Km/h" });
            lc.RelatedData.Add(new RelatedDataItem() { Key = "Evidence of roadworks", Value = "no" });
            location.LocationCategories.Add(lc);
            lm.Locations.Add(location);
            ViewData.Model = lm;
            return View();
        }

        public ActionResult UserManagement()
        {
            List<ADUser> users = new List<ADUser>();
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain);
            foreach (MembershipUser user in GetADUsers())
            {
                UserPrincipal principal = UserPrincipal.FindByIdentity(ctx, user.UserName);
                if (!string.IsNullOrEmpty(principal.Surname) && !string.IsNullOrEmpty(principal.GivenName))
                {
                    //TODO: Determine this from the database
                    bool isUser = (principal.GivenName == "Adam" && principal.Surname == "Burgess") ? true : false;
                    List<UserRole> roles = new List<UserRole>();
                    if (isUser)
                    {
                        roles.Add(new UserRole() { RoleName = "CDR User" });
                        roles.Add(new UserRole() { RoleName = "CDR Manager" });
                        roles.Add(new UserRole() { RoleName = "CDCRC User" });
                    }

                    users.Add(new ADUser()
                    {
                        GivenName = principal.GivenName,
                        Surname = principal.Surname,
                        EmailAddress = principal.EmailAddress,
                        VoiceTelephoneNumber = principal.VoiceTelephoneNumber,
                        UserId = principal.SamAccountName,
                        SID = principal.Sid.ToString(),
                        IsUser = isUser,
                        Roles = roles
                    });
                }
            }
            //CDR User, CDR Manager, CDCRC User, CDCRC Manager, ABS, Internal Audit
            ViewBag.Roles = GetUserRolesToAdd();
            ViewData.Model = new UserManagement() { ADUsers = users };
            return View();
        }

        private List<SelectListItem> GetUserRolesToAdd()
        {
            List<SelectListItem> rolelist = new List<SelectListItem>();
            rolelist.Add(new SelectListItem() { Text = "CDCRC Manager", Value = "1" });
            rolelist.Add(new SelectListItem() { Text = "ABS", Value = "2" });
            rolelist.Add(new SelectListItem() { Text = "Internal Audit", Value = "3" });
            return rolelist;
        }

        public MembershipUserCollection GetADUsers()
        {
            //NB: This is limited to the CCYPCG OU and domain users by the LDAP connection string in the web.config.
            return Membership.GetAllUsers();
        }

        #region "Ajax methods"
        [GridAction]
        public ActionResult _GetUserRolesToAdd()
        {
            return View(new GridModel(GetUserRolesToAdd()));
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AddUserRole()
        {
            return View(new GridModel(GetUserRolesToAdd()));
        }

        [HttpPost]
        [GridAction]
        public ActionResult _DeleteUserRole()
        {
            return View(new GridModel(GetUserRolesToAdd()));
        }

        #endregion
    }
}
