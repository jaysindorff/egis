﻿using System.Data;
using System.Linq;
using System.Web.Mvc;
using EGIS.Models;
using Telerik.Web.Mvc;

namespace EGIS.Controllers
{
    public class AnalyticsForUserController : Controller
    {
        private DataModelContainer db = new DataModelContainer();

        //
        // GET: /vInformationElementsForReview/

        public ViewResult Index()
        {
            Analytics analytics = new Analytics() {
                                                    InformationElementsForReview = db.vInformationElementsForReview.ToList(),
                                                    OpenCases = db.vCaseSummary.ToList(),
                                                    IssuesActions = db.vIssuesActions.ToList()
                                                  };
            ViewData.Model = analytics;
            return View();
        }

        //
        // GET: /vInformationElementsForReview/Details/5

        public ViewResult Details(string id)
        {
            vInformationElementsForReview vinformationelementsforreview = db.vInformationElementsForReview.Find(id);
            return View(vinformationelementsforreview);
        }

        //
        // GET: /vInformationElementsForReview/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /vInformationElementsForReview/Create

        [HttpPost]
        public ActionResult Create(vInformationElementsForReview vinformationelementsforreview)
        {
            if (ModelState.IsValid)
            {
                db.vInformationElementsForReview.Add(vinformationelementsforreview);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vinformationelementsforreview);
        }

        //
        // GET: /vInformationElementsForReview/Edit/5

        public ActionResult Edit(string id)
        {
            vInformationElementsForReview vinformationelementsforreview = db.vInformationElementsForReview.Find(id);
            return View(vinformationelementsforreview);
        }

        //
        // POST: /vInformationElementsForReview/Edit/5

        [HttpPost]
        public ActionResult Edit(vInformationElementsForReview vinformationelementsforreview)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vinformationelementsforreview).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vinformationelementsforreview);
        }

        //
        // GET: /vInformationElementsForReview/Delete/5

        public ActionResult Delete(string id)
        {
            vInformationElementsForReview vinformationelementsforreview = db.vInformationElementsForReview.Find(id);
            return View(vinformationelementsforreview);
        }

        //
        // POST: /vInformationElementsForReview/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            vInformationElementsForReview vinformationelementsforreview = db.vInformationElementsForReview.Find(id);
            //db.vInformationElementsForReview.Remove(vinformationelementsforreview)vInformationElementsForReview
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            //db.Dispose();
            base.Dispose(disposing);
        }

        #region "Ajax methods"
        [GridAction]
        public ActionResult _InformationElements()
        {
            return View(new GridModel(db.vInformationElementsForReview.ToList())); 
        }
        #endregion
    }
}