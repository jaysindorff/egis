/////////////////////////////////////////////////////////////////////
// Author: Sen Chung
// Version: 1.0
// Date:
// Details: This script turns a table into a line graph
////////////////////////////////////////////////////////////////////
Highcharts.visualizeColumn = function(table, options) {
	// the categories
	
	options.xAxis.categories = [];
	$('tbody th', table).each( function(i) {
		options.xAxis.categories.push(this.innerHTML);
	});
	
	// the data series
	options.series = [];
	$('tr', table).each( function(i) {
		var tr = this;
		$('th, td', tr).each( function(j) {
			if (j > 0) { // skip first column
				if (i == 0) { // get the name and init the series
						
					options.series[j - 1] = { 
						
						name: this.innerHTML,
						data: []
					};
				} else { // add values
					options.series[j - 1].data.push(parseFloat(this.innerHTML));
				}
			}
		});
	});
	
	var chart = new Highcharts.Chart(options);
}

$(document).ready(function () {
    var table = '';
    var graphClassName = '';
    var graphTableId = ''; // Name of the tables ID
    var titleText = ''; //Name of the tables Title
    var xAxisText = ''; // Name of the tables xAxis
    var yAxisText = ''; // Name of the tables yAxis
    var legendEnabled = ''; // Do we show a legend?
    var graphPosition = ''; // Add the placeholder for the dynamic graphs
    var styleName = '';

    $('table.columnGraph').each(function (i) {
        var captionClass = $(this).children('caption').attr('class');
        var toolValueSuffix = "";
        var yAxisTextValue = "";
        var xAxisTextValue = "";
        var titleTextValue = "";
        var verticalText = $(this).children('tbody').attr('class');
        var bla = "";

        if (verticalText == 'vertical-text') {
            verticalText = -90;
            alignType = "right";
        } else if (verticalText == 'diagonal-text') {
            verticalText = -45;
            alignType = "right";
        } else {
            verticalText = 0;
            alignType = "center";
        }

        switch (captionClass) {
            case 'percentage':
                toolValueSuffix = '%';
                maxValue = 100;
                minValue = 0;
                break;
            case 'partial-percentage':
                toolValueSuffix = '%';
                maxValue = null;
                minValue = 0;
                break;
            case 'meters':
                toolValueSuffix = 'M';
                maxValue = null;
                minValue = null;
                break;
            default:
                toolValueSuffix = '';
                maxValue = null;
                minValue = 0;
        }

        graphClassName = $(this).attr('class');
        $(this).attr('id', graphClassName + i);
        graphTableId = $(this).attr('id');

        /*
        if ($('table#' + graphTableId + ' caption').has('span.yAxisTextValue')) {
            yAxisTextValue = $('table#' + graphTableId + ' caption span.yAxisTextValue').text();
        } else {
            yAxisTextValue = $('table#' + graphTableId + ' caption').text();
        }

        if ($('table#' + graphTableId + ' caption').has('span.xAxisTextValue')) {
            xAxisTextValue = $('table#' + graphTableId + ' caption span.xAxisTextValue').text();
        } else {
            xAxisTextValue = "";
        }

        if ($('table#' + graphTableId + ' caption').has('span.titleTextValue')) {
            titleTextValue = $('table#' + graphTableId + ' caption span.titleTextValue').text();
        } else {
            titleTextValue = "";
        }
        */
        var tableHeadLength = $('table#' + graphTableId + ' thead th').length;
        if (tableHeadLength < 3) {
            legendEnabled = false;
        } else {
            legendEnabled = true;
        }

        $('table#' + graphTableId).addClass('remove');
        yAxisText = ""; // yAxisTextValue;
        titleText = "";// titleTextValue;
        xAxisText = ""//xAxisTextValue;
        styleName = $('table#' + graphTableId + ' caption').attr('class');
        graphPosition = '<div id="' + graphTableId + '_graph" class="' + styleName + ' dynamic-graphs"></div>';

        $('table#' + graphTableId).after(graphPosition);

        $('table#' + graphTableId + ' caption').removeAttr('class');
        table = document.getElementById(graphTableId),
		options = {
		    chart: {
		        renderTo: graphTableId + '_graph',
		        borderRadius: 0,
		        defaultSeriesType: 'column'
		    },
		    colors: ['#873795', '#ef3a5a', '#f58220', '#00a5e3', '#c39bca', '#f79cac', '#fac08f', '#7fd2f1', '#e7d7ea', '#fcd8de', '#fde6d2', '#ccedf9', '#873795', '#ef3a5a', '#f58220', '#00a5e3', '#c39bca', '#f79cac', '#fac08f', '#7fd2f1', '#e7d7ea', '#fcd8de', '#fde6d2', '#ccedf9'],
		    title: {
		        text: '',
		        style: {
		            color: '#394A58'
		        }
		    },
		    xAxis: {
		        title: {
		            text: xAxisText,
		            style: {
		                color: '#394A58'
		            }
		        },
		        labels: {
		            rotation: verticalText,
		            align: alignType
		        }
		    },
		    yAxis: {
		        title: {
		            text: yAxisText,
		            style: {
		                color: '#394A58'
		            }
		        },
		        labels: {
		            formatter: function () {
		                return this.value + toolValueSuffix
		            }
		        },
		        lineWidth: 1,
		        lineColor: '#C0D0E0',
		        max: maxValue,
		        min: minValue
		    },
		    legend: {
		        enabled: legendEnabled
		    },
		    tooltip: {
		        enabled: false,
		        formatter: function () {
		            return '' + this.x + ': ' + this.y + toolValueSuffix;
		        }
		    },
		    plotOptions: {
		        column: {
		            dataLabels: {
		                enabled: true,
		                formatter: function () {
		                    return this.y + toolValueSuffix;
		                }
		            },
		            pointPadding: 0.1,
		            borderWidth: 0
		        }
		    },
		    credits: {
		        enabled: false
		    }
		};

        Highcharts.visualizeColumn(table, options);

    });
});